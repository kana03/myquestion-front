import axios from 'axios';

async function getNameCategory(ids) {
    let result = '';
    await axios.get('http://localhost:8000/category', {
        params: {
            id_category: ids
        }
    }).then(response => {
        if(response.status === 200) {
            result = response.data.data;
        }

    }).catch((err)=>{
        console.log(err)
        this.err = "Une erreur est survenue lors de la récupération des réponses";

        if(err.response.status === 401) {
            this.err = "Vous n'avez pas l'autorisation de récupérer les réponses";
        }
        return this.err;
    });
    return result;
}

export default {
    getNameCategory
}
