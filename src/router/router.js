import Vue from "vue";
import Router from "vue-router";

import Menu from "@/views/menu";
import Signup from "@/views/signup";
import Signin from "@/views/signin";
import CreateGame from "@/views/create-game";
import Game from "@/views/game";
import Score from "@/views/score";
import localStorageService from "@/services/localStorageService";


Vue.use(Router);

const router = new Router({
    routes: [
        { path: "/", name: "home", redirect: { name: "login" } },
        { path: "/sign-in", name: "login", component: Signin },
        { path: "/sign-up", name: "register", component: Signup },
        { path: "/menu", name: "menu", component: Menu, meta: { requiresAuth: true } },
        { path: "/create-game", name: "create-game", component: CreateGame, meta: { requiresAuth: true } },
        { path: "/game", name: "game", component: Game, meta: { requiresAuth: true } },
        { path: "/score", name: "score", component: Score, meta: { requiresAuth: true } }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const token = localStorageService.getAccessToken()
        if (!token) {
            next({
                name: 'login',
                query: 'sign-in',

            })
        }

        const parsedJwt = localStorageService.parseToken(token)
        if (Math.floor(Date.now() / 1000) > parsedJwt.exp) {
            localStorageService.clearToken()
            next({
                name: 'login',
                query: 'sign-in',
            })
        }
    }
    next()
});

export default router;